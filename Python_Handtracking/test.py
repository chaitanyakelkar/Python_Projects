import cv2
import mediapipe as mp
import time
import numpy as np
import HandTrackingModule as htm

detector = htm.handDetector(detectioncon=0.7)
pTime = 0

cap = cv2.VideoCapture(0)

while True:
    sucess ,img = cap.read()
    img = detector.findHands(img)
    ldmlist = detector.findPosition(img, draw=False)
    if len(ldmlist) != 0:
        cv2.line(img, (ldmlist[4][1],ldmlist[4][2]), (ldmlist[8][1],ldmlist[8][2]), (0,255,0), 3)

    cTime = time.time()
    fps = 1/(cTime-pTime)
    pTime = cTime

    cv2.putText(img, str(int(fps)), (10,70), cv2.FONT_HERSHEY_SCRIPT_SIMPLEX, 2, (255,0,0), 3)

    cv2.imshow("Image", img)
    cv2.waitKey(1)