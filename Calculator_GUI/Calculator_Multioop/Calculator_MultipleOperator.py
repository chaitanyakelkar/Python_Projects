from tkinter import *
from tkinter import font
import tkinter.messagebox


root = Tk()
new_window = Toplevel(root)

root.title("Calculator")
new_window.title("History")
new_window.withdraw()


def click_event(number):
    e.config(state=NORMAL)
    text = e.get()
    e.delete(0, END)
    e.insert(0, text + str(number))
    e.config(state=DISABLED)

    
def equal():
    try:
        result = eval(e.get())
        with open('History.txt','a+') as f:
            f.write(f"{e.get()} = {result} \n")
        e.config(state=NORMAL)
        e.delete(0, END)
        e.insert(0, result)
        e.config(state=DISABLED)
    except SyntaxError:
        tkinter.messagebox.showerror("Invalid Syntax",f"Syntax error in {e.get()}")
        e.config(state=NORMAL)
        e.delete(0, END)
        e.config(state=DISABLED)


def history():
    new_window.deiconify()
    with open('History.txt',"r") as f:
        his = f.read()
    text.config(text=his)


def ok():
    new_window.withdraw()


def clear():
    e.config(state=NORMAL)
    e.delete(0, END)
    e.config(state = DISABLED)


p_font = font.Font(size=20)
text = Message(new_window, font=p_font)
e = Entry(font=p_font, width=40, state=DISABLED)

text.grid(row=0)
e.grid(row=0, columnspan=3, ipady=10)

button_1 = Button(height=2, width=12, text="1", command=lambda: click_event(1), font=p_font)
button_2 = Button(height=2, width=12, text="2", command=lambda: click_event(2), font=p_font)
button_3 = Button(height=2, width=12, text="3", command=lambda: click_event(3), font=p_font)
button_4 = Button(height=2, width=12, text="4", command=lambda: click_event(4), font=p_font)
button_5 = Button(height=2, width=12, text="5", command=lambda: click_event(5), font=p_font)
button_6 = Button(height=2, width=12, text="6", command=lambda: click_event(6), font=p_font)
button_7 = Button(height=2, width=12, text="7", command=lambda: click_event(7), font=p_font)
button_8 = Button(height=2, width=12, text="8", command=lambda: click_event(8), font=p_font)
button_9 = Button(height=2, width=12, text="9", command=lambda: click_event(9), font=p_font)
button_0 = Button(height=2, width=12, text="0", command=lambda: click_event(0), font=p_font)

button_clear = Button(height=2, width=12, text="Clear", command=clear, font=p_font)
button_add = Button(height=2, width=12, text="+", command=lambda: click_event("+"), font=p_font)
button_subtract = Button(height=2, width=12, text="-", command=lambda: click_event("-"), font=p_font)
button_multiply = Button(height=2, width=12, text="x", command=lambda: click_event("*"), font=p_font)
button_divide = Button(height=2, width=12, text="/", command=lambda: click_event("/"), font=p_font)
button_exponential = Button(height=2, width=12, text="Exponent", command=lambda: click_event("**"), font=p_font)
button_remainder = Button(height=2, width=12, text="remainder", command=lambda: click_event("%"), font=p_font)
button_equal = Button(height=2, width=12, text="=", command=equal, font=p_font)
button_history = Button(height=2, width=37, text="History", command=history, font=p_font)
button_ok = Button(new_window,text="OK",command=ok, font=p_font)


button_1.grid(row=1, column=0)
button_2.grid(row=1, column=1)
button_3.grid(row=1, column=2)
button_4.grid(row=2, column=0)
button_5.grid(row=2, column=1)
button_6.grid(row=2, column=2)
button_7.grid(row=3, column=0)
button_8.grid(row=3, column=1)
button_9.grid(row=3, column=2)
button_0.grid(row=4, column=0)

button_add.grid(row=4, column=1)
button_subtract.grid(row=4, column=2)
button_multiply.grid(row=5, column=0)
button_clear.grid(row=5, column=2)
button_divide.grid(row=6, column=0)
button_remainder.grid(row=5, column=1)
button_exponential.grid(row=6, column=1)
button_equal.grid(row=6, column=2)
button_history.grid(row=7, columnspan=3)
button_ok.grid(row=1)

root.mainloop()
